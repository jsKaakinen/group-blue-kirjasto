import React from "react";

const Footer = () => {
    return (
        <section className="footer">
            <p>&#169; Blue Library 2023</p>
        </section>
    );
}

export default Footer;