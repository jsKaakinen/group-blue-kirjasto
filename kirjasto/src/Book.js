import axios from "axios";
import {Link} from 'react-router-dom';

function Book({curUser, book, books, setBooks, users, setUsers}) {
    const userIndex = users.findIndex(user => user.name === curUser); //find current user index
    const userId =  userIndex !== -1 ? users[userIndex].id : null;
    const thisBook = books.find(elem => elem.id === book.id)
    function borrowBook(copyNum) {
        const dueDate = new Date(); //get new date
        dueDate.setDate(dueDate.getDate() + 30); //new date +30days
      
        const updatedBook = {...thisBook}; //modify book to borrow state
        const copyIndex = updatedBook.copies.findIndex(copy => copy.id === copyNum.id);
        updatedBook.copies[copyIndex].status = 'borrowed';
        updatedBook.copies[copyIndex].due_date = dueDate.toISOString();
        updatedBook.copies[copyIndex].borrower_id = users[userIndex].id;
      
        const updatedUser = { ...users[userIndex] };
        updatedUser.books_history.unshift(
          updatedBook.copies[copyIndex].id
        );
        
        axios //update book in database
            .put(`http://localhost:3001/books/${book.id}`, updatedBook)
            .then(res => {
              setBooks(prevBooks => prevBooks.map(b => b.id === book.id ? updatedBook : b));
            })
            .catch(error => {
              console.error(error);
            });

        axios //update user in database
            .put(`http://localhost:3001/users/${users[userIndex].id}`, updatedUser)
            .then(res => {
              const updatedUsers = [...users];
              updatedUsers[userIndex] = updatedUser;
              setUsers(updatedUsers);
            })
            .catch(error => {
              console.error(error);
            });
    }

    function returnBook(copyNum) {
        const updatedBook = {...thisBook}; //modify book to unborrowed state
        const copyIndex = updatedBook.copies.findIndex(copy => copy.id === copyNum.id);
        updatedBook.copies[copyIndex].status = 'in_library';
        updatedBook.copies[copyIndex].due_date = null;
        updatedBook.copies[copyIndex].borrower_id = null;

        axios //update book in database
            .put(`http://localhost:3001/books/${book.id}`, updatedBook)
            .then(res => {
              setBooks(prevBooks => prevBooks.map(b => b.id === book.id ? updatedBook : b));
            })
            .catch(error => {
              console.error(error);
            });
    }
    
    return (
        <div id='Book'> 
        <hr/>
            <h2>{book.author}</h2>
            <h1>{book.title}</h1>
            <h2>{book.subtitle}</h2>
            <h5>{book.published.slice(0,4)}</h5>
            <p id="bookdescription">{book.description}</p>
            <h5>ISBN: {book.isbn}</h5>
            <div>
                <h3>Copies in library:</h3>
                {book.copies.map(item =>  { 
                    if (item.status === 'borrowed' && userId === item.borrower_id) { //if book is borrowed an user.id matches book copy id
                        return (<div id='borrowed'>
                            <p>You have this book until {item.due_date.slice(0,10)}</p>
                            <button onClick={() => returnBook(item)}>Return the book</button> 
                        </div>)}
                    if (item.status === 'borrowed' && userId !== item.borrower_id) { //if book is borrowed
                        return (<div id='borrowed'>
                            <p>Borrowed until {item.due_date.slice(0,10)}</p> 
                        </div>)}  
                    if (item.status === 'in_library') { //if book is in available.
                        if (curUser !== 'Guest') { //if curUser is logged on
                            return (<div id='in_library_logged'>
                                <p>In library</p>
                                <button onClick={() => borrowBook(item)}>Borrow</button>
                            </div>)
                        }
                        if (curUser === 'Guest' || curUser === undefined) { //if user is guest
                            return (<div id='in_library_guest'>
                                <p>In library</p>
                                {/* <button className='btn'>Login ph</button> */}
                                <Link id='login_link'to='/login'>Login to borrow</Link>
                            </div>)
                        }
                    }
                    return (<p>error</p>)
                })}
            </div>
        </div>
    )
}

export default Book;