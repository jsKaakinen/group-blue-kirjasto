import React, { useState } from 'react';

function Logout({curUser, setCurUser}) {
    const [show, setShow] = useState(false);
    const [showName, setShowName] = useState(curUser);
    
    function handleClick(e) {
        e.preventDefault();
        console.log("Testi11");
        setShow(true);
        setCurUser("Guest");
    }
    return (
        <div id="container3">
            <div className="signupForm">
            <button class="btn" onClick={handleClick} type="submit">Logout</button>
            {show && <p>{showName} you are now logged out. Thank you for visiting Blue Library!</p>}
            </div>
        </div>
    );
}

export default Logout;