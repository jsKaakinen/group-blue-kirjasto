import Book from "./Book";

function BookLister({books, curUser}) {
    return (
        <>
        {books.map(item => {
            return  (<Book book={item} curUser={curUser}/>)
        })}
        </>
    )
}

export default BookLister;