
function Frontpage(){
    return (
        <>
        <section class="container"> 
            <video id="v" loop="true" autoplay="autoplay" controls muted>
                <source src="https://trello.com/1/cards/63ca7ace84da49014f9f027e/attachments/63d7af18f60c8854b4dfb9dd/download/Library_-_846.mp4" type="video/mp4"></source>
            </video>
            <div class="overlay">
                <p id="overlay">Amazing Blue Library Software</p>
                
            </div>
        </section>
        <section class="section">
            <div class="box-container2">
                <div>
                    <h1>Welcome</h1>
                    <p>We are a local library, dedicated to providing easy access to a diverse 
                        collection of physical books, DVDs, and other resources. Our aim is to 
                        meet the needs and interests of our community by offering a variety of 
                        materials, programs, and services. Our welcoming environment is the perfect 
                        place to discover new books, explore new ideas, and connect with others. 
                        Whether you are looking to borrow materials, attend events, or just find 
                        a quiet place to study, our library is here to support you. So come visit 
                        us today and see what we have to offer!</p>
                </div>
                <div class="box2">
                <img src="https://trello.com/1/cards/63ca7ace84da49014f9f027e/attachments/63ce79e587001e003c4de9df/download/open-book-1428428_640.jpg" 
                    alt="kirja" className="kirja" />
                </div>
            </div>
        </section>
        <section id="section2">
            <h2>Search Books</h2>
            <button id="frtbutton"><a href="booksearch"><b>Book Search</b></a></button>
        </section>
        <section>
        <iframe title="kartta" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6729.557741785338!2d25.448467299738113!3d65.05988999361365!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46802d0f3732fa09%3A0x54f77f1c43b91c44!2sTeknologiantie%202%2C%2090590%20Oulu!5e0!3m2!1sfi!2sfi!4v1675249228454!5m2!1sfi!2sfi" 
            width="100%" height="450" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
        </section>
        </>
    )
}

export default Frontpage;
