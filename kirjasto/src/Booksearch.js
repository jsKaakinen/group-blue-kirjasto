import React, {useState} from 'react';
import "./Booksearch.css";
import Book from "./Book";


function Booksearch({books, curUser, users, setUsers, setBooks}) {
    let result = "";
    let isbn = books.map(book => book.isbn);
    let title = books.map(book => book.title);
    let author = books.map(book => book.author);
    let id = null;
    let titleName = "";

    function titleSearch(searchString) {
        for(let i = 0; i < title.length; i++) {
            if (title[i].toLowerCase().includes(searchString)) {
                id = i;
                return title[i];
            } 
            
            else {
                result = false;
            }
        }
            return result;
    }
    function authorSearch(searchString) {
        const titlesFound = [];
        for(let i = 0; i < author.length; i++) {
            let authorFound = searchString.includes(author[i].toLowerCase());
            if(authorFound === true) {
                titlesFound.push(title[i])
                id = i;
            }
        }
        if(titlesFound.length === 0) {
            return false;
        }
        else {
            
            return titlesFound;
        }
    }
    function isbnSearch(searchString) {
        for(let i = 0; i < isbn.length; i++) {
            let isbnFound = searchString.includes(isbn[i]);
            if(isbnFound === true) {
                id = i;
                return title[i];
            }
        }
        return false; 
    }

    const [searchString, setSearchString] = useState("");
    const [submitState, setSubmitState] = useState("");
    const [show, setShow] = useState(false);
    
    function onChange(e) {

        setSubmitState(e.target.value);
       
    }
    function handleClick(e) {
        e.preventDefault();
        convertTextToLowerCase();       
        setShow(true);
        
    }
    function convertTextToLowerCase() {
        //To convert Lower Case
        let text = submitState.toLowerCase();
        setSearchString(text);
        
      };
    const titleFound = titleSearch(searchString);
    const titleFound2 = authorSearch(searchString);
    const titleFound3 = isbnSearch(searchString);
    
    if(titleFound !== false){
        titleName = titleFound;
    }
    else if(titleFound2 !== false) {
        titleName = titleFound2;
    }
    else if(titleFound3 !== false) {
        titleName = titleFound3;
    }
    else {
        titleName = "Book not found";
        
    }    
    
    return (
    <div id='container'>
    
     
            <section class="slider"/>
            <h3>Search for books</h3>
            <form>  
                <p>Search: &nbsp;                                          
                <input type="text" onChange={onChange} placeholder="Search.." ></input> 
                <button class="btn" onClick={handleClick} type="submit">Submit</button> </p>
            </form>
            {show && id === null && <p id="notfound">{titleName}</p>} 

            {show && id !== null && <Book curUser={curUser} books={books} book={books[id]} setBooks={setBooks} users={users} setUsers={setUsers}/>}
            
    </div>
        
       
    )
    
};
export default Booksearch;