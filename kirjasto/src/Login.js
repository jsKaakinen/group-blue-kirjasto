import React, { useState } from 'react';
import MyPage from './MyPage';

function LoginForm({users, curUser, setCurUser}) {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [errorMessage, setErrorMessage] = useState('');
  const [succesful, setSuccessful] = useState(false);

  let user2 = users.map(user => user.name);
  let password2 = users.map(passwd => passwd.password);
  let userfound = false;
  let index = 0;

  function userExist(username) {
    console.log("name test");
    for(let i = 0; i < user2.length; i++) {
      if(username === user2[i]) {
        userfound = true;
        index = i;
        passwordCorrect(password, username);
        break;
      } else {
          userfound = false;
          setErrorMessage('Username not found!');
        }
           
    }
  }
  function passwordCorrect(password, username) {
    if(password2[index] === password) {
      setSuccessful(true);
      setErrorMessage("Login succesful!")
      setCurUser(username);
      console.log(curUser);
    
    } else {
      setSuccessful(false);
      setErrorMessage("Password is not correct!");
    }

  }
  const handleSubmit = (event) => {
    event.preventDefault();
    let isFormValid = true;
    userExist(username);

    if (!username) {
      isFormValid = false;
      setErrorMessage('Username is required');
    }
    if (!password) {
      isFormValid = false;
      setErrorMessage('Password is required');
    }
  }

  return (
    <div id="container3">
      <div className="signupForm">
        <form classname="login-form"onSubmit={handleSubmit}>
          
          <h2>Login</h2>
          <label>
            Username: <br/>
            <input type="text" value={username} onChange={(event) => setUsername(event.target.value)} />
          </label>
          <br />
          <label>
            Password: <br/>
            <input type="password" value={password} onChange={(event) => setPassword(event.target.value)} />
          </label>
          <br />
          
          
          <input type="submit" value="Login" />

          <h3>OR:</h3>
          <button class="facebook-button">
          <span class="facebook-logo"></span>Login with Facebook</button>
          <br/><br/>
          <button class="google-button">
          <span class="google-logo"></span>Login with Google</button>
        </form><br/>
        {errorMessage && <div className="error-message">{errorMessage}</div>}
        {/* {succesful && <p>Login successful!</p>} */}
      </div>
    </div>
  );
  
}

export default LoginForm;