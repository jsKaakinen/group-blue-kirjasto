import { useState } from 'react';
import axios from 'axios';

 
function SignupForm() {
    const [name, setName] = useState('');
    const [password, setPassword] = useState('');
    const [users, setUsers] = useState([]);
    const [books_history, setBooks_history] = useState([]);
    const [submitted, setSubmitted] = useState(false);
    const [error, setError] = useState(false);

    const handleName = (e) => {
        setName(e.target.value);
        setSubmitted(false);
    };

    const handlePassword = (e) => {
        setPassword(e.target.value);
        setSubmitted(false);
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        if (name === '' || password === '') {
            setError(true);
        } else {
            newUser(name, password, books_history)
            setSubmitted(true);
            setError(false);
            
        }
    };

    const toLoginPage = () => {
        return (
            <>
                {window.location = '/Login'}
            </>
        );
    }

    const newUser = () => {
        axios.post('http://localhost:3001/users/', { name: name, password: password, 
            books_history: books_history, admin: false })
        .then((response) => {
            setName("");
            setPassword("");
            setBooks_history([]);
            setUsers([...users, response.data]);
            setTimeout(toLoginPage, 4000);
        });
    }

    const successMessage = () => {
        return (
            <div className="success" style={{
            display: submitted ? '' : 'none',}}>
            <h1>User {name} successfully registered!</h1>
            </div>
        );
    };

    const errorMessage = () => {
        return (
            <div className="error" style={{
            display: error ? '' : 'none',}}>
            <h1>Fill all the fields</h1>
            </div>
        );
    };

    const resetForm = () => {
        setName("");
        setPassword("");
    }

    return (
        <div id="container2">
            <div className="signupForm">
                <h2>Signup</h2>
                {errorMessage()}
                {successMessage()}
            
                <form>
                    <label className="label">Name:</label><br/>
                    <input onChange={handleName} className="input"
                    value={name} type="text" /><br/>

                    <label className="label">Password:</label><br/>
                    <input onChange={handlePassword} className="input"
                    value={password} type="password" /><br/><br/>

                    <button onClick={handleSubmit} className="btn" type="submit">Submit</button>&ensp;
                    <button onClick={resetForm} className="btn2">Reset</button>

                    <h3>OR:</h3>
                    <button class="facebook-button">
                    <span class="facebook-logo"></span>
                    Login with Facebook
                    </button>
                    <br/><br/>
                    <button class="google-button">
                    <span class="google-logo"></span>
                    Login with Google
                    </button>
                </form>
            </div>
        </div>
    );
}

export default SignupForm;
         